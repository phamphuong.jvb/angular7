import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { enableProdMode } from '@angular/core';

//Modules
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr'; 

// Services
import { AuthService } from './services/auth/auth.service';
import { UserService } from './services/user/user.service';
import { StudentService } from './services/student/student.service';
import { ProductsService } from './services/products/products.service';
import { RestApiService } from './services/shared/rest-api.service';
// Pipes
import { FilterPipe } from './pipes/filter.pipe';
import { PhonePipe } from './pipes/phone.pipe';

// Components
import { AppComponent } from './components/index/app.component';
import { StudentListComponent } from './components/student/list/student-list.component';
import { StudentDetailsComponent } from './components/student/details/student-details.component';
import { StudentAddComponent } from './components/student/add/student-add.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { HighlightStudentDirective } from './directives/highlight-student.directive';
import { ProductsAddComponent } from './components/products/add/products-add.component';
import { ProductsDetailsComponent } from './components/products/details/products-details.component';
import { ProductsListComponent } from './components/products/list/products-list.component';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { homeChildRoutes } from '../app/app-routing.module';

import { EmployeeCreateComponent } from './components/employees/employee-create/employee-create.component';
import { EmployeesListComponent } from './components/employees/employees-list/employees-list.component';
import { EmployeeEditComponent } from './components/employees/employee-edit/employee-edit.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import {DemoMaterialModule} from './material-module';
// Parent Routes
const routes : Routes = [
{
	path: '',
	component: HomeComponent,
	children :homeChildRoutes,
	// canActivate : [AuthService]
},
{
	path: 'login',
	component: LoginComponent
},
{
	path: '**',
	redirectTo: ''
}
];

@NgModule({
	declarations: [
	AppComponent,
	StudentListComponent,
	StudentDetailsComponent,
	StudentAddComponent,
	LoginComponent,
	HomeComponent,
	RegisterComponent,
	FilterPipe,
	PhonePipe,
	HighlightStudentDirective,
	ProductsAddComponent,
	ProductsDetailsComponent,
	ProductsListComponent,
	EmployeeCreateComponent,
	EmployeesListComponent,
	EmployeeEditComponent
	],
	imports: [
	BrowserModule,
	RouterModule,
	RouterModule.forRoot(routes),
	FormsModule,
	HttpClientModule,
	HttpModule,
	ReactiveFormsModule,
	BrowserAnimationsModule,
	ToastrModule.forRoot({ 
		timeOut: 3000,
		positionClass: 'toast-bottom-right',
		preventDuplicates: true,
	}),
	MatButtonModule,
	MatCheckboxModule,
	DemoMaterialModule
	],
	providers: [AuthService,UserService,StudentService,ProductsService,RestApiService],
	bootstrap: [AppComponent]
})

// enableProdMode();

export class AppModule { }
