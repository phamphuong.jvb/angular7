
 import { Component, OnInit } from '@angular/core';
 import { RouterModule, Routes ,Router} from '@angular/router';
 import { ToastrService } from 'ngx-toastr';

 // Components
 import { StudentListComponent } from './components/student/list/student-list.component';
 import { StudentDetailsComponent } from './components/student/details/student-details.component';
 import { StudentAddComponent } from './components/student/add/student-add.component';

 // Services
 import { routerTransition } from './services/config/config.service';
import { ProductsAddComponent } from './components/products/add/products-add.component';
import { ProductsDetailsComponent } from './components/products/details/products-details.component';
import { ProductsListComponent } from './components/products/list/products-list.component';


import { EmployeeCreateComponent } from './components/employees/employee-create/employee-create.component';
import { EmployeesListComponent } from './components/employees/employees-list/employees-list.component';
import { EmployeeEditComponent } from './components/employees/employee-edit/employee-edit.component';


 // Define and export child routes of HomeComponent
 export const homeChildRoutes : Routes = [
    {
        path: '',
        component: StudentListComponent
    },
    {
        path: 'add',
        component: StudentAddComponent
    },
    {
        path: 'update/:id',
        component: StudentAddComponent
    },
    {
        path: 'detail/:id',
        component: StudentDetailsComponent
    },
    {
       path: 'list',
       component: ProductsListComponent
   },
    {
       path: 'addproducts',
       component: ProductsAddComponent
   },
   {
       path: 'updateproducts/:id',
       component: ProductsAddComponent
   },
   {
       path: 'detailproducts/:id',
       component: ProductsDetailsComponent
   },
//    employees
{
    path: 'listemp',
    component: EmployeesListComponent
},
 {
    path: 'addemp',
    component: EmployeeCreateComponent
},
{
    path: 'updateemp/:id',
    component: EmployeeEditComponent
},
];
   