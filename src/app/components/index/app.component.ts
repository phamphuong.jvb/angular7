/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */

import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})


export class AppComponent {
	title = 'Student Management By Sangwin Gawande';

	// Add few students for initial listing
	studentsList = [
	{	
		id : 1,
		first_name : "Sangwin",
		last_name : "Gawande",
		email : "sangwin@yopmail.com",
		phone : 9503733178,
		department : "Science",
		image : "assets/b2.jpg"
	},
	{
		id : 2,
		first_name : "Oman",
		last_name : "Umir",
		email : "oman@yopmail.com",
		phone : 8574889658,
		department : "Commerce",
		image : "assets/b3.jpg"
	},
	{
		id : 3,
		first_name : "Tina",
		last_name : "Dillon",
		email : "tina@yopmail.com",
		phone : 7485889658,
		department : "Science",
		image : "assets/b1.jpg"
	},
	{
		id : 4,
		first_name : "John",
		last_name : "Doe",
		email : "john@yopmail.com",
		phone : 9685589748,
		department : "Arts",
		image : "assets/b2.jpg"
	},
	{
		id : 5,
		first_name : "Peter",
		last_name : "Parker",
		email : "peter@yopmail.com",
		phone : 8595856547,
		department : "Engineering",
		image : "assets/b3.jpg"
	}
	];
	ProductsList = [
		{
		name: 'sp1',
		descripbe : 'Get to work',
		type : 'type 1'
		},
		{
		name: 'sp 2',
		descripbe : 'Pick up groceries',
		type : 'type 2'
		},
		{
		name: 'sp 3',
		descripbe : 'Go home',
		type : 'type 3'
		},
		{
		name: 'sp 4',
		descripbe : 'Fall asleep',
		type : 'type 4'
		},
	];

	done = [
		{
			name: 'sp5',
			descripbe : 'Get up',
			type : 'type 5'
			},
			{
			name: 'sp 6',
			descripbe : 'Brush teeth',
			type : 'type 6'
			},
			{
			name: 'sp 7',
			descripbe :	'Take a shower',
			type : 'type 7'
			},
			{
			name: 'sp 8',
			descripbe : 'Walk dog',
			type : 'type 8'
			},
  ];

  
	constructor() {
		// Save students to localStorage
		localStorage.setItem('students', JSON.stringify(this.studentsList));
		localStorage.setItem('done', JSON.stringify(this.done));
		localStorage.setItem('ProductsList', JSON.stringify(this.ProductsList));
	}
}

/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */
