import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/services/products/products.service';
import { ToastrService } from 'ngx-toastr';
import { routerTransition } from '../../../services/config/config.service';
@Component({
  selector: 'app-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ProductsListComponent implements OnInit {
 
 
  ProductsList:any;
  ProductsListData:any;
  constructor(private ProductsService:ProductsService,private toastr: ToastrService) { 
  
  }
  // Call Products list function on page load 
  ngOnInit() {
    this.getProductsList();
  }

  //Get Products list from services
  getProductsList(){
    let ProductsList = this.ProductsService.getAllProducts();
    this.success(ProductsList)
  }

  //Get Products list success
  success(data){
    this.ProductsListData = data.data;
    for (var i = 0; i < this.ProductsListData.length; i++) {
      this.ProductsListData[i].name
    }
  }

  // Delete a Products with its index
  deleteProducts(index:number){
    // get confirm box for confirmation
    let r = confirm("Are you sure?");
    if (r == true) {
      let ProductsDelete = this.ProductsService.deleteProduct(index);
      if(ProductsDelete) {
        this.toastr.success("Success", "Products Deleted");
      } 
      this.getProductsList();
    }
  }
}
