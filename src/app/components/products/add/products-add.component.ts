import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray,transferArrayItem} from '@angular/cdk/drag-drop';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import { RouterModule, Routes ,Router,ActivatedRoute} from '@angular/router';

// Services
import { ValidationService } from '../../../services/config/config.service';
import { ProductsService } from '../../../services/products/products.service';
import { routerTransition } from '../../../services/config/config.service';

import { ToastrService } from 'ngx-toastr';
import { HttpClient,HttpEventType } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
@Component({
  selector: 'app-add',
  templateUrl: './products-add.component.html',
  styleUrls: ['./products-add.component.css'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ProductsAddComponent implements OnInit {

  ngOnInit() {
  }


  constructor(private formBuilder: FormBuilder,private router: Router, private route: ActivatedRoute, private productsService:ProductsService,private toastr: ToastrService) {
    this.route.params.subscribe(params => {
      this.index = params['id'];
      // check if ID exists in route & call update or add methods accordingly
      if (this.index && this.index != null && this.index != undefined) {
        this.getProductDetails(this.index);
      }else{
        this.createForm(null);
      }
    });
  }


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }

	private productsAddForm : FormGroup;
 	index:any;
  // Submit products details form
  doRegister(){
    if (this.index && this.index != null && this.index != undefined) {
      this.productsAddForm.value.id = this.index;
      
    }else{
      this.index = null;
    }
    let productsRegister = this.productsService.doRegisterProduct(this.productsAddForm.value, this.index);
    if(productsRegister) {
      if (productsRegister.code == 200) {
        this.toastr.success(productsRegister.message,"Success");
        this.router.navigate(['/']);
      }else{
        this.toastr.error(productsRegister.message,"Failed");
      }
    }
  }
  
 	// If this is update form, get user details and update form
 	getProductDetails(index:number){
    let studentDetail = this.productsService.getProductDetails(index);
    this.createForm(studentDetail);
  }
  // If this is update request then auto fill form
  createForm(data){
    if (data == null) {
      this.productsAddForm = this.formBuilder.group({
        
        name: ['',  [Validators.required,Validators.minLength(3),Validators.maxLength(50)]],
        descripbe: ['',  [Validators.required,Validators.minLength(3),Validators.maxLength(50)]],
        type: ['',  [Validators.required]]
      });			
    }else{
      this.productsAddForm = this.formBuilder.group({
   
        name: ['',  [Validators.required,Validators.minLength(3),Validators.maxLength(50)]],
        descripbe: ['',  [Validators.required,Validators.minLength(3),Validators.maxLength(50)]],
        type: ['',  [Validators.required]]
      });
    }
  }
}
