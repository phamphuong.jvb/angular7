import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import { RouterModule, Routes ,Router,ActivatedRoute} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

// Services
import { ProductsService } from '../../../services/Products/Products.service';
import { routerTransition } from '../../../services/config/config.service';
@Component({
  selector: 'app-details',
  templateUrl: './products-details.component.html',
  styleUrls: ['./products-details.component.css'],
  animations: [routerTransition()],
  host: {'[@routerTransition]': ''}
})
export class ProductsDetailsComponent implements OnInit {
  index:any;
  ProductsDetail:any;
  constructor(private router: Router, private route: ActivatedRoute, private ProductsService:ProductsService,private toastr: ToastrService) { 
    // Get user detail index number sent in params
    this.route.params.subscribe(params => {
      this.index = params['id'];
      if (this.index && this.index != null && this.index != undefined) {
        this.getProductsDetails(this.index);
      }
    });
  }

  ngOnInit() {
  }

  // Get Products details 
  getProductsDetails(index:number){
    let getProductsDetail = this.ProductsService.getProductDetails(index);
    if(getProductsDetail) {
      this.ProductsDetail = getProductsDetail.ProductData;
      this.toastr.success(getProductsDetail.message,"Success");
    }
  }

}
