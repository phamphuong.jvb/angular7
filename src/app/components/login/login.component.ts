/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */

import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes ,Router ,ActivatedRoute} from '@angular/router';
import { ValidationService } from '../../services/config/config.service';
import { UserService } from '../../services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { routerTransition } from '../../services/config/config.service';
import { first } from 'rxjs/operators';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css'],
	animations: [routerTransition()],
	host: {'[@routerTransition]': ''}
})
export class LoginComponent implements OnInit {
	private loginForm : FormGroup;
	loading = false;
    submitted = false;
    returnUrl: string;
	constructor(private formBuilder: FormBuilder,private router: Router, 
		private route: ActivatedRoute,
		private userService:UserService,private toastr: ToastrService) { 
		this.loginForm = this.formBuilder.group({
			email: ['',  [Validators.required, ValidationService.emailValidator]],
			password: ['',[Validators.required, ValidationService.passwordValidator]]
		});
	}

    ngOnInit() {
        // reset login status
        this.userService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    doLogin() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.login(this.f.email.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    this.loading = false;
                });
    }
	// Check if user already logged in
	// ngOnInit() {
	// 	if(localStorage.getItem('userData')) {
	// 		this.router.navigate(['/']);
	// 	}
	// }

	// // Initicate login
	// doLogin(){
	// 	let login = this.userService.doLogin(this.loginForm.value);
	// 	this.success(login);
	// }

	// // Login success function
	// success(data){
	// 	if (data.code == 200) {
	// 		localStorage.setItem('userData', JSON.stringify(data.data));
	// 		this.router.navigate(['/']);
	// 		this.toastr.success('Success', "Logged In Successfully");
	// 	}else{
	// 		this.toastr.error('Failed', "Invalid Credentials");
	// 	}
	// }

}

/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */