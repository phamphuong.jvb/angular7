/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */

 import { Injectable } from '@angular/core';
 @Injectable()
 export class ProductsService {
   http: any;
  

   // Get all Products list via API or any data storage
   getAllProducts(){
     let ProductsList:any;
     if(localStorage.getItem('Products') && localStorage.getItem('Products') != '') {
       ProductsList = {
         code : 200,
         message : "Products List Fetched Successfully",
         data : JSON.parse(localStorage.getItem('Products'))
       }
     }else{
       ProductsList = {
         code : 200,
         message : "Products List Fetched Successfully",
         data : JSON.parse(localStorage.getItem('Products'))
       }
     }
     return ProductsList;
   }

   doRegisterProduct(data, index){
     let ProductsList = JSON.parse(localStorage.getItem('Products'));
     let returnData;
     console.log("index", index);
     if(index != null) {


       for (var i = 0; i < ProductsList.length; i++) {
         if (index != i && ProductsList[i].email == data.email) {
           returnData = {
             code : 503,
             message : "Email Address Already In Use",
             data : null
           }    
           return returnData;
         }
       }

       ProductsList[index] = data;
       localStorage.setItem('Products',JSON.stringify(ProductsList));
       returnData = {
         code : 200,
         message : "Product Successfully Updated",
         data : JSON.parse(localStorage.getItem('Products'))
       }    
     }else{      
       data.id = this.generateRandomID();
       for (var i = 0; i < ProductsList.length; i++) {
         if (ProductsList[i].email == data.email) {
           returnData = {
             code : 503,
             message : "Email Address Already In Use",
             data : null
           }    
           return returnData;
         }
       }
       ProductsList.unshift(data);

       localStorage.setItem('Products',JSON.stringify(ProductsList));

       returnData = {
         code : 200,
         message : "Product Successfully Added",
         data : JSON.parse(localStorage.getItem('Products'))
       }    
     }
     return returnData;
   }

   deleteProduct(index:number){
     let ProductsList = JSON.parse(localStorage.getItem('Products'));

     ProductsList.splice(index, 1);

     localStorage.setItem('Products',JSON.stringify(ProductsList));

     let returnData = {
       code : 200,
       message : "Product Successfully Deleted",
       data : JSON.parse(localStorage.getItem('Products'))
     }

     return returnData;
   }



   getProductDetails(index:number){
     let ProductsList = JSON.parse(localStorage.getItem('Products'));

     let returnData = {
       code : 200,
       message : "Product Details Fetched",
       ProductData : ProductsList[index]
     }

     return returnData;
   }


   generateRandomID() {
     var x = Math.floor((Math.random() * Math.random() * 9999));
     return x;
   }

 }
/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */
