/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */

import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders ,HttpParams} from '@angular/common/http';
import { User } from '../../_models';
import { map } from 'rxjs/operators';


@Injectable()
export class UserService {
	apiURL = 'http://localhost:3000';
	constructor(private http: HttpClient) { }
	// Http Options

    getAll() {
        return this.http.get<User[]>(this.apiURL + `/login`);
    }

    getById(id: number) {
        return this.http.get(this.apiURL + `/login` + id);
    }

    register(user: User) {
        return this.http.post(this.apiURL + `apiURL/login/register`, user);
    }

    update(user: User) {
        return this.http.put(this.apiURL + `/login` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(this.apiURL + `/login` + id);
	}
	
	login(email: string, password: string) {
		let params = new  HttpParams({
			fromObject :{
				email: email, 
			password: password
			}
		});
		const Options = {
			userlog : params,
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			}),
		}
        return this.http.get<any>(this.apiURL + `/login`, Options)
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));
    }
    // login(email: string, password: string) {
    //     return this.http.post<any>(this.apiURL + `/login`, { email: email, password: password })
    //         .pipe(map(user => {
    //             // login successful if there's a jwt token in the response
    //             if (user && user.token) {
    //                 // store user details and jwt token in local storage to keep user logged in between page refreshes
    //                 localStorage.setItem('currentUser', JSON.stringify(user));
    //             }

    //             return user;
    //         }));
    // }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
	// constructor() { }

	// doLogin(data){
	// 	if (data.email == "admin@gmail.com" && data.password == "admin123") {
	// 		return {
	// 			code : 200,
	// 			message : "Login Successful",
	// 			data : data
	// 		};
	// 	}else{
	// 		return {
	// 			code : 503,
	// 			message : "Invalid Credentials",
	// 			data : null
	// 		};
	// 	}
	// }

	// doRegister(data){
		// 	return this.http.post('user-add.php',data);	
		// }
	}

/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */