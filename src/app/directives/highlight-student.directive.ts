/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */

import { Directive, ElementRef,HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlightStudent]'
})
export class HighlightStudentDirective {

	constructor(private el: ElementRef) {
	}

	@HostListener('mouseenter') onMouseEnter() {
		this.highlight('#009688','#fff');
	}

	@HostListener('mouseleave') onMouseLeave() {
		this.highlight(null ,null);
	}

	private highlight(color: string,colortx:string) {
		this.el.nativeElement.style.backgroundColor = color;
		this.el.nativeElement.style.color = colortx;
	}
}

/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */