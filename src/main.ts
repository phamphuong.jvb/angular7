// import './polyfills';

// import {HttpClientModule} from '@angular/common/http';
// import {NgModule} from '@angular/core';
// import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// import {MatNativeDateModule} from '@angular/material';
// import {BrowserModule} from '@angular/platform-browser';
// import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// import {DemoMaterialModule} from './material-module';

// import {AppComponent} from './app/components/index/app.component';
// import {AppModule} from './app/app.module'
// @NgModule({
//   imports: [
//     BrowserModule,
//     BrowserAnimationsModule,
//     FormsModule,
//     HttpClientModule,
//     DemoMaterialModule,
//     MatNativeDateModule,
//     ReactiveFormsModule,
//     AppModule
//   ],
//   entryComponents: [AppComponent],
//   declarations: [AppComponent],
//   bootstrap: [AppComponent],
//   providers: []
// })
// export class AppMainModule {}

// platformBrowserDynamic().bootstrapModule(AppMainModule);
/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import 'hammerjs';
if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));

/**
 * Created By : Sangwin Gawande (http://sangw.in)
 */